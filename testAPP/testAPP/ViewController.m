//
//  ViewController.m
//  testAPP
//
//  Created by 王迪 on 2017/6/19.
//  Copyright © 2017年 stock. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, assign) NSInteger successNum;
@property (nonatomic, assign) NSInteger failNum;

@property (nonatomic, assign) BOOL isKnow;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSInteger count = 10000;
    
    _isKnow = YES;
    
    for (int i = 0; i < count; i ++) {
        
        NSInteger chooseNum = arc4random_uniform(4) + 1;
        
        if ([self test3:chooseNum]) {
            _successNum ++;
        } else {
            _failNum ++;
        }
    }
    NSLog(@"%zd",_successNum);

}

- (BOOL)test1:(NSInteger)chooseNum {
    if (chooseNum == 1) {
        return YES;
    }
    return NO;
}

- (BOOL)test2:(NSInteger)chooseNum {
    
    if (chooseNum == 1) {
        return NO;
    }
    
    NSNumber *choseN = [NSNumber numberWithInteger:chooseNum];
    
    NSMutableArray *numArray = @[@1,@2,@3,@4].mutableCopy;
    
    [numArray removeObject:choseN];
    
    if (chooseNum == 4) {
        [numArray removeObject:@3];
    } else {
        [numArray removeObject:@4];
    }
    
    NSInteger chooseIndex = arc4random_uniform(2);
    
    NSNumber *newNum = numArray[chooseIndex];
    
    return newNum.integerValue == 1;
}

- (BOOL)test3:(NSInteger)chooseNum {

    if (chooseNum == 1) {
        return NO;
    }
    
    NSNumber *choseN = [NSNumber numberWithInteger:chooseNum];
    
    NSMutableArray *numArray = @[@1,@2,@3,@4].mutableCopy;
    
    [numArray removeObject:choseN];
    
    [numArray removeObjectAtIndex:arc4random_uniform(3)];
 
    NSNumber *newNum = numArray[arc4random_uniform(2)];
    
    return newNum.integerValue == 1;
}











@end
