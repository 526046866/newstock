//
//  MomentUnreadCountAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/3/30.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface MomentUnreadCountAPI : APIRequest

@property (nonatomic, strong) NSString *res_code;

@end
