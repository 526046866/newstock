//
//  DgzqMessageAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/6/7.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface DgzqMessageAPI : APIRequest

@property (nonatomic, copy) NSString *message;

@end
