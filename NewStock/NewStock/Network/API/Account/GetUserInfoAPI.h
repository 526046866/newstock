//
//  GetUserInfoAPI.h
//  NewStock
//
//  Created by Willey on 16/7/22.
//  Copyright © 2016年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface GetUserInfoAPI : APIRequest

@property (nonatomic, strong) NSString *userId;

@end
