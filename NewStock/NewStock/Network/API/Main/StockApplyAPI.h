//
//  StockApplyAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/5/2.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface StockApplyAPI : APIRequest

@property (nonatomic, copy) NSString *st;

@end
