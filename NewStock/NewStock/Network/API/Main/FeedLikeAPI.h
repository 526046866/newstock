//
//  FeedLikeAPI.h
//  NewStock
//
//  Created by Willey on 16/12/1.
//  Copyright © 2016年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface FeedLikeAPI : APIRequest

@property (nonatomic, strong) NSString *fId;

@end
