//
//  ThemeDetailAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/5/8.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface ThemeDetailAPI : APIRequest

@property (nonatomic, strong) NSString *ids;

@end
