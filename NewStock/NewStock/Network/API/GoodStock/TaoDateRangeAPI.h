//
//  TaoDateRangeAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/2/24.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface TaoDateRangeAPI : APIRequest

@property (nonatomic, strong) NSNumber *count;

@property (nonatomic, copy) NSString *n;

@end
