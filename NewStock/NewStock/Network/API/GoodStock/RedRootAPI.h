//
//  RedRootAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/2/13.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface RedRootAPI : APIRequest

@property (nonatomic, copy) NSString *d;

@property (nonatomic, copy) NSString *code;

@end
