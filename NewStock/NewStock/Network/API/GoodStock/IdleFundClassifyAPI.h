//
//  IdleFundClassifyAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/2/15.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface IdleFundClassifyAPI : APIRequest

@property (nonatomic, copy) NSString *pcode;

@end
