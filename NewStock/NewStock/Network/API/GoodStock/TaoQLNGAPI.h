//
//  TaoQLNGAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/6/22.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface TaoQLNGAPI : APIRequest

@property (nonatomic, strong) NSString *ids;

@end
