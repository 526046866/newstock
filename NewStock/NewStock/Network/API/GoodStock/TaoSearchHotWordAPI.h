//
//  TaoSearchHotWordAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/2/27.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"
#import "Defination.h"

@interface TaoSearchHotWordAPI : APIRequest

@property (nonatomic, copy) NSString *code;

@end
