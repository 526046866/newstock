//
//  TaoSearchHotPeopleAPI.h
//  NewStock
//
//  Created by 王迪 on 2017/3/6.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "APIRequest.h"

@interface TaoSearchHotPeopleAPI : APIRequest

@property (nonatomic, copy) NSString *code;

@end
