//
//  UIImage+SnapWindow.h
//  text
//
//  Created by 王迪 on 2016/12/4.
//  Copyright © 2016年 JiaBei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (SnapWindow)

+ (UIImage *)snapTheCurrentWindow;

@end
