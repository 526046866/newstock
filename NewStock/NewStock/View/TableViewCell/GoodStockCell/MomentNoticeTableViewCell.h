//
//  MomentNoticeTableViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/3/31.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MomentNoticeModel.h"

@interface MomentNoticeTableViewCell : UITableViewCell

@property (nonatomic, strong) MomentNoticeModel *model;

@end
