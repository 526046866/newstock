//
//  TaoContinueLimitCatchTableViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/6/21.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaoContinueLimitCatchTableViewCell : UITableViewCell

- (void)setN:(NSString *)n s:(NSString *)s t:(NSString *)t zx:(NSString *)zx zdf:(NSString *)zdf r:(NSString *)r dayCount:(NSString *)dayCount limitCount:(NSString *)limitCount ;

@end
