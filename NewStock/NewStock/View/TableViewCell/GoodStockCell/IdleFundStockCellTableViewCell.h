//
//  IdleFundStockCellTableViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/2/15.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IdleFundStockModel.h"

@interface IdleFundStockCellTableViewCell : UITableViewCell

@property (nonatomic, strong) IdleFundStockListModel *model;

@end
