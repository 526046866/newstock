//
//  TaoIndexBlockTableViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/3/10.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaoIndexModel.h"

@interface TaoIndexBlockTableViewCell : UITableViewCell

@property (nonatomic, strong) TaoIndexModelClildStock *model;

@end
