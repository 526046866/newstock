//
//  TaoCommedTableViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/6/23.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaoCommedTableViewCell : UIView

- (CGFloat)setUserIcon:(NSString *)icon n:(NSString *)n c:(NSString *)c ;

@property (nonatomic, assign) CGFloat height;

@end
