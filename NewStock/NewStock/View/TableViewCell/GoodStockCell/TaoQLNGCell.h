//
//  TaoQLNGCell.h
//  NewStock
//
//  Created by 王迪 on 2017/6/27.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaoQLNGCell : UITableViewCell

- (CGFloat)setUserIcon:(NSString *)icon n:(NSString *)n c:(NSString *)c tm:(NSString *)tm;

@property (nonatomic, assign) CGFloat height;

@end
