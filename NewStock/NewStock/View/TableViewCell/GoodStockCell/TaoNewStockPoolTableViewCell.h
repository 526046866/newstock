//
//  TaoNewStockPoolTableViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/6/21.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaoNewStockPoolTableViewCell : UITableViewCell

- (void)setN:(NSString *)n s:(NSString *)s t:(NSString *)t zx:(NSString *)zx zdf:(NSString *)zdf tr:(NSString *)tr isOpen:(NSString *)isOpen openCount:(NSString *)openCount ;

@end
