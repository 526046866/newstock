//
//  TaoSearchPPlCell.h
//  NewStock
//
//  Created by 王迪 on 2017/3/8.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TaoSearchPPlModel.h"

@interface TaoSearchPPlCell : UITableViewCell

@property (nonatomic, strong) TaoSearchPPlListModel *model;

@end
