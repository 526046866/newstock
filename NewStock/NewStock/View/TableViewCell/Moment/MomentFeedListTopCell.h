//
//  MomentFeedListTopCell.h
//  NewStock
//
//  Created by 王迪 on 2017/3/15.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedListModel.h"

@interface MomentFeedListTopCell : UITableViewCell

@property (nonatomic, strong) FeedListModel *model;

@end
