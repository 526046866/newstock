//
//  AccountCenterTableViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/4/12.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountCenterTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *imageStr;

@property (nonatomic, strong) NSString *leftStr;

@property (nonatomic, strong) NSString *rightStr;

@end
