//
//  StockApplyTableViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/5/3.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StockApplyModel.h"

@interface StockApplyTableViewCell : UITableViewCell

@property (nonatomic) StockApplyModelList *model;

@end
