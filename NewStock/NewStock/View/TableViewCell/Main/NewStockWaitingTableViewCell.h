//
//  NewStockWaitingTableViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/6/29.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewStockWaitingTableViewCell : UITableViewCell

- (void)setName:(NSString *)name code:(NSString *)code price:(NSString *)price tm:(NSString *)tm rate:(NSString *)rate ;



@end
