//
//  MainTopAdCollectionViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/3/17.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTopAdCollectionViewCell : UICollectionViewCell

@property (nonatomic, copy) NSString *imgUrl;

@end
