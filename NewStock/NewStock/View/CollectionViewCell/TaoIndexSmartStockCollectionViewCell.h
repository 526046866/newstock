//
//  TaoIndexSmartStockCollectionViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/6/19.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TaoIndexSmartStockCollectionViewCell : UICollectionViewCell

- (void)setUrl:(NSString *)url name:(NSString *)name ico:(NSString *)ico;

@end
