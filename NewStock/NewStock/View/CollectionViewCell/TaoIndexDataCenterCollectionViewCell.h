//
//  TaoIndexDataCenterCollectionViewCell.h
//  NewStock
//
//  Created by 王迪 on 2017/6/19.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TaoIndexDataCenterCollectionViewCell : UICollectionViewCell

- (void)setUrl:(NSString *)url ico:(NSString *)ico name:(NSString *)name;

@end
