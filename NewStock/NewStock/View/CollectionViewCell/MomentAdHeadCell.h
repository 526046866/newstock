//
//  MomentAdHeadCell.h
//  NewStock
//
//  Created by 王迪 on 2017/5/15.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdListItemModel.h"

@interface MomentAdHeadCell : UICollectionViewCell

@property (nonatomic, strong) AdListItemModel *model;

@end
