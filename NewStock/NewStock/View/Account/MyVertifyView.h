//
//  MyVertifyView.h
//  NewStock
//
//  Created by 王迪 on 2017/4/19.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyVertifyInfoModel.h"

@interface MyVertifyView : UIView

@property (nonatomic, strong) MyVertifyInfoModel *model;

@end
