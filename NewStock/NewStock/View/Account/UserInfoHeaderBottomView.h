//
//  UserInfoHeaderBottomView.h
//  NewStock
//
//  Created by 王迪 on 2017/4/13.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoHeaderBottomView : UIView

@property (nonatomic, assign) BOOL hasList;

@end
