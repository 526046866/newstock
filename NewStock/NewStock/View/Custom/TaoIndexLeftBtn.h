//
//  TaoIndexLeftBtn.h
//  NewStock
//
//  Created by 王迪 on 2017/3/10.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaoIndexLeftBtn : UIButton

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *imgStr;

@property (nonatomic, copy) NSString *imgURL;

@property (nonatomic, strong) UILabel *titleLb;

@end
