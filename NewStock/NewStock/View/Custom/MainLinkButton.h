//
//  MainLinkButton.h
//  NewStock
//
//  Created by 王迪 on 2017/3/17.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainLinkButton : UIButton

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *imgStr;

@property (nonatomic, copy) NSString *imgURL;

@end
