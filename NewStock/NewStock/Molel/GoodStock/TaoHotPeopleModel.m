//
//  TaoHotPeopleModel.m
//  NewStock
//
//  Created by 王迪 on 2017/3/6.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "TaoHotPeopleModel.h"

@implementation TaoHotPeopleModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"n":@"n",
             @"p":@"p",
             @"k":@"k",
             };
}

@end
