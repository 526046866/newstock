//
//  TaoDepartmentInfoModel.m
//  NewStock
//
//  Created by 王迪 on 2017/2/27.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "TaoDepartmentInfoModel.h"

@implementation TaoDepartmentInfoModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {

    return @{
             @"n":@"n",
             @"s":@"s",
             };
}

@end
