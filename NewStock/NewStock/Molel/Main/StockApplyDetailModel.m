//
//  StockApplyDetailModel.m
//  NewStock
//
//  Created by 王迪 on 2017/6/29.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "StockApplyDetailModel.h"

@implementation StockApplyDetailModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"n":@"n",
             @"s":@"s",
             @"sd":@"sd",
             @"pd":@"pd",
             @"lr":@"lr",
             @"tm":@"tm",
             @"pr":@"pr",
             @"pe":@"pe",
             @"iv":@"iv",
             @"mx":@"mx",
             @"iiv":@"iiv",
             @"tl":@"tl",
             @"desc":@"desc",
             };
}

@end

@implementation StockApplyDetailValueModel



@end
