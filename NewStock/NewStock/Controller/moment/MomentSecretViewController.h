//
//  MomentSecretViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/3/23.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface MomentSecretViewController : BaseViewController

@property (nonatomic, assign) NSInteger unreadCount;

@end
