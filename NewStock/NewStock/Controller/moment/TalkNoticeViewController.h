//
//  TalkNoticeViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/3/31.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface TalkNoticeViewController : BaseViewController

@property (nonatomic, copy) NSString *res_code;

@end
