//
//  QuickNewsViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/5/23.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface QuickNewsViewController : BaseViewController

- (void)scrollToTop;

- (void)timerRefresh;

@end
