//
//  MyStockOptionalViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/6/12.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface MyStockOptionalViewController : BaseViewController

@property (nonatomic, assign) NSInteger pushIndex;

@end
