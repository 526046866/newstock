//
//  MyHoldStockViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/1/5.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "EmbedBaseViewController.h"

@interface MyHoldStockViewController : EmbedBaseViewController<UITableViewDelegate,UITableViewDataSource>

@end
