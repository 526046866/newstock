//
//  MyStockAnnounceListViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/6/9.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface MyStockAnnounceListViewController : BaseViewController

- (void)scrollToTop;

@end
