//
//  RedRootViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/2/13.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface RedRootViewController : BaseViewController

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *code;

@end
