//
//  IdleFundViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/2/14.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface IdleFundViewController : BaseViewController

@property (nonatomic, copy) NSString *name;

@end
