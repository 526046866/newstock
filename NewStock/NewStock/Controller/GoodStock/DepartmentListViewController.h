//
//  DepartmentListViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/2/24.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface DepartmentListViewController : BaseViewController

@property (nonatomic, strong) NSArray *dataArray;

@end
