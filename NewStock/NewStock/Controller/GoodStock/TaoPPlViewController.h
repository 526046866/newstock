//
//  TaoPPlViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/3/7.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface TaoPPlViewController : BaseViewController

@property (nonatomic, copy) NSString *stockName;
@property (nonatomic, copy) NSString *t;
@property (nonatomic, copy) NSString *s;
@property (nonatomic, copy) NSString *m;
@property (nonatomic, copy) NSString *n;

@end
