//
//  TaoSearchStockViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/2/22.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface TaoSearchStockViewController : BaseViewController

@property (nonatomic, copy) NSString *d;
@property (nonatomic, copy) NSString *s;
@property (nonatomic, copy) NSString *t;
@property (nonatomic, copy) NSString *m;
@property (nonatomic, copy) NSString *n;

@end
