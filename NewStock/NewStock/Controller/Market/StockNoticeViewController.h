//
//  StockNoticeViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/6/20.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface StockNoticeViewController : BaseViewController

@property (nonatomic, copy) NSString *t;
@property (nonatomic, copy) NSString *s;
@property (nonatomic, copy) NSString *m;

@end
