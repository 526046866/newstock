//
//  StockApplyDetailViewController.h
//  NewStock
//
//  Created by 王迪 on 2017/6/29.
//  Copyright © 2017年 Willey. All rights reserved.
//

#import "BaseViewController.h"

@interface StockApplyDetailViewController : BaseViewController

@property (nonatomic, strong) NSString *n;
@property (nonatomic, strong) NSString *s;


@end
