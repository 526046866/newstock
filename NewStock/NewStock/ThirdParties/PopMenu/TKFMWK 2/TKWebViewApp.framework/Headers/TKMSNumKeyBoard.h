//
//  TKMSNumKeyBoard.h
//  TKKeyboardDemo
//
//  Created by liupm on 16/5/4.
//  Copyright © 2016年 liupm. All rights reserved.
//

#import "TKBaseKeyBoard.h"

/**
 *  @author 刘宝, 2016-05-08 14:05:35
 *
 *  招商版本的数字键盘
 */
@interface TKMSNumKeyBoard : TKBaseKeyBoard

@end
