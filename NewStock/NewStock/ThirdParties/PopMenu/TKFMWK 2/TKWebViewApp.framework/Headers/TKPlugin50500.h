//
//  TKPlugin50500.h
//  TKAppBase_V2
//
//  Created by 刘宝 on 17/1/13.
//  Copyright © 2017年 com.thinkive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKBasePlugin.h"

/**
 *  @Author 刘宝, 2015-07-24 01:07:39
 *
 *  统一模块交互消息定义
 */
@interface TKPlugin50500 : TKBasePlugin

@end
