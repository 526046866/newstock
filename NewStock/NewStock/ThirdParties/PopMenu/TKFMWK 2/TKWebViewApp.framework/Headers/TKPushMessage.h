//
//  TKPushMessage.h
//  TKAppBase_V2
//
//  Created by 刘宝 on 17/1/13.
//  Copyright © 2017年 com.thinkive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKUtil.h"

/**
 *  @author 刘宝, 2017-01-13 08:01:18
 *
 *  推送消息
 */
@interface TKPushMessage : DynModel

/**
 *  @author 刘宝, 2017-01-13 08:01:03
 *
 *  消息ID
 */
@property (nonatomic,copy)NSString *msgId;

/**
 *  @author 刘宝, 2017-01-13 08:01:03
 *
 *  消息类型
 */
@property (nonatomic,copy)NSString *msgType;

/**
 *  @author 刘宝, 2017-01-13 08:01:50
 *
 *  消息日期
 */
@property (nonatomic,copy)NSString *createDate;

/**
 *  @author 刘宝, 2017-01-13 08:01:25
 *
 *  消息时间
 */
@property (nonatomic,copy)NSString *createTime;

/**
 *  @author 刘宝, 2017-01-13 08:01:55
 *
 *  消息主题ID
 */
@property (nonatomic,copy)NSString *topicId;

/**
 *  @author 刘宝, 2017-01-13 08:01:31
 *
 *  消息在改主题下的索引
 */
@property (nonatomic,assign)int msgPos;

@end
