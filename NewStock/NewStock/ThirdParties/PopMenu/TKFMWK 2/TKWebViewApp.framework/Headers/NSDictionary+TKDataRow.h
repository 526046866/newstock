//
//  NSDictionary+TKDataRow.h
//  TKUtil_V1
//
//  Created by 刘宝 on 16/3/24.
//  Copyright © 2016年 liubao. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  DataRow
 */
@interface NSDictionary (TKDataRow)

/**
 *  获取字符串
 *
 *  @param key
 *
 *  @return
 */
-(NSString *)getStringWithKey:(NSString *)key;

/**
 *  获取int
 *
 *  @param key
 *
 *  @return
 */
-(int)getIntWithKey:(NSString *)key;

/**
 *  获取Long
 *
 *  @param key
 *
 *  @return
 */
-(long)getLongWithKey:(NSString *)key;

/**
 *  获取Long Long
 *
 *  @param key
 *
 *  @return
 */
-(long long)getLongLongWithKey:(NSString *)key;

/**
 *  获取float
 *
 *  @param key
 *
 *  @return
 */
-(float)getFloatWithKey:(NSString *)key;

/**
 *  获取double
 *
 *  @param key
 *
 *  @return
 */
-(double)getDoubleWithKey:(NSString *)key;

/**
 *  获取数字
 *
 *  @param key
 *
 *  @return
 */
-(NSInteger)getIntegerWithKey:(NSString *)key;

/**
 *  获取Number
 *
 *  @param key
 *
 *  @return
 */
-(NSNumber *)getNumberWithKey:(NSString *)key;

/**
 *  获取Bool
 *
 *  @param key
 *
 *  @return
 */
-(BOOL)getBoolWithKey:(NSString *)key;

/**
 *  获取对象
 *
 *  @param key
 *
 *  @return
 */
-(NSObject *)getObjectWithKey:(NSString *)key;

@end
