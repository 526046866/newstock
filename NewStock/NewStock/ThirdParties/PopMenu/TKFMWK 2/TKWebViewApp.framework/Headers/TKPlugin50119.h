//
//  TKPlugin50119.h
//  TKAppBase_V2
//
//  Created by liubao on 16-3-18.
//  Copyright (c) 2016年 com.thinkive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKBasePlugin.h"

/**
 *  @Author 刘宝, 2015-08-06 20:08:43
 *
 *  设置浏览器状态栏颜色
 */
@interface TKPlugin50119 : TKBasePlugin

@end
