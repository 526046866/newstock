//
//  TKPlugin50118.h
//  TKAppBase_V2
//
//  Created by liubao on 16-2-25.
//  Copyright (c) 2016年 com.thinkive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKBasePlugin.h"

/**
 *  @Author 刘宝, 2015-08-06 20:08:43
 *
 *  代理发送http/https相关的网络请求
 */
@interface TKPlugin50118 : TKBasePlugin

@end
