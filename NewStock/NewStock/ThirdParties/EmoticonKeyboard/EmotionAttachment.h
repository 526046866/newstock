//
//  EmotionAttachment.h
//  oc-emotion
//
//  Created by 王迪 on 2017/2/9.
//  Copyright © 2017年 JiaBei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmotionAttachment : NSTextAttachment

@property (nonatomic ,copy) NSString *chs;

@end
