//
//  EmotionModel.h
//  oc-emotion
//
//  Created by 王迪 on 2017/2/9.
//  Copyright © 2017年 JiaBei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmotionModel : NSObject

@property (nonatomic, copy) NSString *chs;

@property (nonatomic, copy) NSString *png;

@property (nonatomic, strong) NSMutableAttributedString *nmAttrStr;

@end
