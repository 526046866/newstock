//
//  EmotionCell.h
//  oc-emotion
//
//  Created by 王迪 on 2017/2/9.
//  Copyright © 2017年 JiaBei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmotionModel.h"

@interface EmotionCell : UICollectionViewCell

@property (nonatomic, strong) NSArray <EmotionModel *> *modelArray;



@end
