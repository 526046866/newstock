//
//  TKPlugin50303.h
//  TKAppBase_V2
//
//  Created by 刘宝 on 17/1/12.
//  Copyright © 2017年 com.thinkive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKBasePlugin.h"

/**
 *  @Author 刘宝, 2015-07-24 01:07:39
 *
 *  老版本统计页面访问次数，停留时长等（H5自己统计页面的进入和退出的时间，通过此接口保存到服务器）
 */
@interface TKPlugin50303 : TKBasePlugin

@end
