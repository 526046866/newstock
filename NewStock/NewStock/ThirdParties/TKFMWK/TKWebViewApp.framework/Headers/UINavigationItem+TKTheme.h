//
//  UINavigationItem+TKTheme.h
//  TKAppBase_V2
//
//  Created by 刘宝 on 16/12/28.
//  Copyright © 2016年 com.thinkive. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  @author 刘宝, 2016-12-28 22:12:45
 *
 *  支持主题
 */
@interface UINavigationItem (TKTheme)

/**
 *  @Author 刘宝, 2015-04-30 00:04:54
 *
 *  设置左边普通文字按钮
 *
 *  @param target 代理类
 *  @param action 代理方法
 *  @param title  文字
 *  @param cssId  样式ID
 */
- (void)setLeftItemWithTarget:(id)target action:(SEL)action title:(NSString *)title cssId:(NSString *)cssId;

/**
 *  @Author 刘宝, 2015-04-30 00:04:21
 *
 *  设置右边文本按钮
 *
 *  @param target 代理类
 *  @param action 代理方法
 *  @param title  文本
 *  @param cssId  样式ID
 */
- (void)setRightItemWithTarget:(id)target action:(SEL)action title:(NSString *)title cssId:(NSString *)cssId;

/**
 *  @Author 刘宝, 2015-04-30 00:04:23
 *
 *  设置后退按钮
 *
 *  @param target 代理类
 *  @param action 代理方法
 *  @param title  文字
 *  @param cssId  样式ID
 */
- (void)setBackItemWithTarget:(id)target action:(SEL)action title:(NSString *)title cssId:(NSString *)cssId;

/**
 *  @Author 刘宝, 2015-04-23 14:04:55
 *
 *  设置导航左侧的按钮
 *
 *  @param target 代理对象
 *  @param action 动作事件
 *  @param title  标题
 *  @param space  间隔
 *  @param cssId  样式ID
 */
- (void)setLeftItemWithTarget:(id)target action:(SEL)action title:(NSString *)title space:(CGFloat)space cssId:(NSString *)cssId;

/**
 *  @Author 刘宝, 2015-04-23 14:04:55
 *
 *  设置导航右侧的按钮
 *
 *  @param target 代理对象
 *  @param action 动作事件
 *  @param title  标题
 *  @param space  间隔
 *  @param cssId  样式ID
 */
- (void)setRightItemWithTarget:(id)target action:(SEL)action title:(NSString *)title space:(CGFloat)space cssId:(NSString *)cssId;

@end
